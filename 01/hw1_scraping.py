#  -*- coding: utf-8 -*-

from datetime import datetime

import numpy as np
import pandas as pd
import sklearn as skit
import matplotlib.pyplot as plt
import seaborn as sns
import requests
import urllib3
import pandas as pd
from bs4 import BeautifulSoup
import time

root_url = 'https://dspace.cvut.cz'
url = root_url + '/recent-submissions?offset='
http = urllib3.PoolManager()

csv_file = 'hw1' + str(int(round(time.time() * 1000))) +'.csv'
print(csv_file)
open(csv_file, 'a').close()

#csv_file = 'hw11541591602237.csv'

cols = ['dc.contributor.author',
        'dc.date.accessioned',
        'dc.date.available',
        'dc.date.issued',
        'dc.date.submitted',
        'dc.identifier',
        'dc.identifier.uri',
        'dc.publisher',
        'dc.type',
        'dc.contributor.referee',
        'theses.degree.name',
        'theses.degree.discipline',
        'theses.degree.grantor',
        'theses.degree.programme',
        'evskp.contact']

#write only header on the start
result = pd.DataFrame(columns=cols)
with open(csv_file, 'a', encoding='utf-8') as f:
    # result.index = result.index.str.encode('utf-8')
    result.to_csv(f, header=True, index=True, encoding="utf-8")


for offset in range(0, 31150, 20):
    result = pd.DataFrame(columns=cols)
    print('scraping: ' + url + str(offset))
    r = requests.get(url + str(offset))
    soup = BeautifulSoup(r.content, features="lxml", from_encoding='utf-8')
    # print(soup.prettify())
    # i need to visit all sub pages to obtain required detials its painfully slow
    for div in soup.find_all('h4', {'class': 'artifact-title'}):
        a = div.find('a')
        link = a.get('href')

        # r = requests.get(root_url + str(link)+ '?show=full')
        # soup = BeautifulSoup(r.content)
        # print(soup.prettify())

        rr = requests.get(root_url + str(link) + '?show=full')
        soup2 = BeautifulSoup(rr.content, features="lxml", from_encoding='utf-8')

        rows = soup2.find("table").find_all("tr")

        #there is problem with mulitple fields author or maybe more
        #i need to concatenate values with same key i know  we shoud use pandas but plain dict will do
        #also i did not need to get rid of locale column in table
        map = dict()
        for c in cols:
            map[c] = None

        for row in rows:
            cells = row.find_all("td")
            key = cells[0].text
            val = cells[1].text
            if (key in cols):
                if (key in map):
                    if map[key] is None:
                        map[key] = val
                    else:
                        map[key] += ',' + str(val)
                else:
                    map[key] = str(val)

        r = len(result.index) + 1
        for key in map.keys():
            result.loc[r, key] = map[key]

    print('appending')
    with open(csv_file, 'a', encoding='utf-8') as f:
        # result.index = result.index.str.encode('utf-8')
        result.to_csv(f, header=False, index=False, encoding="utf-8",sep=';')
