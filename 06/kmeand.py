import numpy as np
import random


# taken from https://github.com/alexminnaar/time-series-classification-and-clustering
def DTWDistance(s1, s2, w):
    DTW = {}

    w = max(w, abs(len(s1) - len(s2)))

    for i in range(-1, len(s1)):
        for j in range(-1, len(s2)):
            DTW[(i, j)] = float('inf')
    DTW[(-1, -1)] = 0

    for i in range(len(s1)):
        for j in range(max(0, i - w), min(len(s2), i + w)):
            dist = (s1[i] - s2[j]) ** 2
            DTW[(i, j)] = dist + min(DTW[(i - 1, j)], DTW[(i, j - 1)], DTW[(i - 1, j - 1)])

    return np.sqrt(DTW[len(s1) - 1, len(s2) - 1])


def LB_Keogh(s1, s2, r):
    LB_sum = 0
    for ind, i in enumerate(s1):

        lower_bound = min(s2[(ind - r if ind - r >= 0 else 0):(ind + r)])
        upper_bound = max(s2[(ind - r if ind - r >= 0 else 0):(ind + r)])

        if i > upper_bound:
            LB_sum = LB_sum + (i - upper_bound) ** 2
        elif i < lower_bound:
            LB_sum = LB_sum + (i - lower_bound) ** 2

    return np.sqrt(LB_sum)


def k_means_clust(data, num_clust, num_iter, w=5):
    idx = np.random.randint(len(data[:]), size=num_clust)
    centroids = data[idx, :]
    counter = 0
    for n in range(num_iter):
        counter += 1
        print(counter)
        assignments = {}
        # assign data points to clusters
        for ind, i in enumerate(data):
            min_dist = float('inf')
            closest_clust = None
            for c_ind, j in enumerate(centroids):
                if LB_Keogh(i, j, w) < min_dist:
                    cur_dist = DTWDistance(i, j, w)
                    if cur_dist < min_dist:
                        min_dist = cur_dist
                        closest_clust = c_ind
            if closest_clust in assignments:
                assignments[closest_clust].append(ind)
            else:
                assignments[closest_clust] = []

        # recalculate centroids of clusters
        for key in assignments:
            clust_sum = [0 for d in data[0]]
            if len(assignments[key])!=0:
                for k in assignments[key]:
                    clust_sum = clust_sum + data[k]
                centroids[key] = [m / len(assignments[key]) for m in clust_sum]

    return centroids


def single_knn(train, X, distnace_method, w, num_neighbors):
    closest_seq = []
    # print ind
    for j in train:
        dist = distnace_method(j[:-1], X[:-1], w)
        closest_seq.append((dist, j[-1]))

    closest_seq.sort(key=lambda tup: tup[1])
    closest_seq = closest_seq[:num_neighbors]
    print(closest_seq)

    result = {}
    for _, c in closest_seq:
        if c not in result:
            result[c] = 0
        result[c] += 1
    max = 0
    c = -1
    for k, v in result.items():
        if v > max:
            max = v
            c = k
    return c


train = np.genfromtxt('train.csv', delimiter='\t')
test = np.genfromtxt('test.csv', delimiter='\t')

centroids = k_means_clust(train[:, :-1], 6, 10)

classified_centroids = {}
for c in centroids:
    cls = single_knn(train, c, DTWDistance, 15, 5)
    classified_centroids[cls] = c
