### here comes your code]
import numpy as np
import pandas as pd
from scipy import stats, optimize
from sklearn import model_selection, linear_model, metrics, preprocessing, feature_selection
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns


def linreg(train, validate, plot=False, train_error=True):
    # Data prepare
    X = train.drop(['SalePrice'], axis=1, errors='ignore')
    y = train.SalePrice
    Xv = validate.drop(['SalePrice'], axis=1, errors='ignore')
    yv = validate.SalePrice

    # Linear Regression train
    clf = linear_model.LinearRegression()
    clf.fit(X, y)

    # Print RMSE
    print('Linear regression root mean squared validation error:',
          np.sqrt(metrics.mean_squared_error(clf.predict(Xv), yv)))
    if train_error:
        print('Linear regression root mean squared train error:',
              np.sqrt(metrics.mean_squared_error(clf.predict(X), y)))

    # Joint Plot
    if plot:
        sns.jointplot(yv, clf.predict(Xv))


def ridgereg(train, validate, plot=False, train_error=True):
    # Prepare functions to automatic alpha determination using cross validation
    def scorer(Y, yp):
        return np.sqrt(metrics.mean_squared_error(Y, yp))

    def ridgemodel(alpha):
        clf = linear_model.Ridge(alpha=alpha)
        return np.mean(model_selection.cross_val_score(clf, X, y, cv=5, scoring=metrics.make_scorer(scorer)))

    # Data prepare
    X = train.drop(['SalePrice'], axis=1, errors='ignore')
    y = train.SalePrice
    Xv = validate.drop(['SalePrice'], axis=1, errors='ignore')
    yv = validate.SalePrice

    # Find Ridge alpha automatically
    opt_alpha = optimize.minimize_scalar(ridgemodel, options={'maxiter': 30}, method='bounded', bounds=(0.1, 400))
    print(opt_alpha)

    # Ridge regression model
    clf = linear_model.Ridge(alpha=opt_alpha.x)
    clf.fit(X, y)

    # Print RMSE
    print('Ridge regression root mean squared validation error:',
          np.sqrt(metrics.mean_squared_error(clf.predict(Xv), yv)))
    if train_error:
        print('Ridge regression root mean squared train error:',
              np.sqrt(metrics.mean_squared_error(clf.predict(X), y)))
    # Joint Plot
    if plot:
        sns.jointplot(yv, clf.predict(Xv))




df = pd.read_csv('hw3_data.csv')
df[df.select_dtypes(include=['object']).columns] = df.select_dtypes(include=['object']).apply(pd.Series.astype,dtype='category')
df.loc[:, df.select_dtypes(include=['float64']).columns] = df.loc[:,df.select_dtypes(include=['float64']).columns].fillna(0)

print(df.columns[df.isnull().any()])

df = pd.get_dummies(df)

df[df.select_dtypes(['float16', 'float64', 'int64']).columns] = df[df.select_dtypes(['float16', 'float64', 'int64']).columns].astype('float64')

print(df.head())
for column in df.filter(regex='Area|SF', axis=1).columns:  # SF - square foot which means area
    df['Has' + column] = (df[column] > 0).replace({True: 1, False: 0}).astype('uint8')
    df['Sqrt' + column] = np.sqrt(df[column])

standard_scaler = preprocessing.StandardScaler()
minmax_scaler = preprocessing.MinMaxScaler()

columns = df.select_dtypes(include=['float64']).columns
columns = columns.drop('SalePrice', errors = 'ignore')
print('Columns to be standardized: ', list(columns))

print('Shape of the data:', df.shape)
dt, dv = model_selection.train_test_split(df, test_size=0.25, random_state=17)
dt = dt.copy()
dv = dv.copy()
print('Train: ', len(dt), '; Validation: ', len(dv))

standard_scaler.fit(dt[columns])
minmax_scaler.fit(dt[columns])

dt_s = dt.copy()
dv_s = dv.copy()
dt_s[columns] = standard_scaler.transform(dt[columns])
dv_s[columns] = standard_scaler.transform(dv[columns])

dt_s[columns] = minmax_scaler.transform(dt_s[columns])
dv_s[columns] = minmax_scaler.transform(dv_s[columns])

columns_to_remove = ['Id']
columns_to_remove = list(set(list(dt_s.columns[dt_s.var() < 0.01]) + columns_to_remove)) # note that after the standardization all non-indicator variables have variance 1


#print(dt_s[columns_to_remove].var().nlargest(5))
print(dt_s[columns_to_remove].var().nsmallest(5))
print(len(columns_to_remove))

corrP = dt_s.drop(columns_to_remove, axis = 1, errors = 'ignore').corr(method='pearson')
corrS = dt_s.drop(columns_to_remove, axis = 1, errors = 'ignore').corr(method='spearman')

corrP_cols = corrP.SalePrice.abs().nlargest(10).index
corrS_cols = corrS.SalePrice.abs().nlargest(10).index

fig, axs = plt.subplots(ncols=2, figsize=(15,7))
sns.heatmap(corrP.abs().loc[corrP_cols,corrP_cols],ax=axs[0], cbar=False, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=corrP_cols.values, xticklabels=corrP_cols.values)
sns.heatmap(corrS.abs().loc[corrS_cols,corrS_cols],ax=axs[1], cbar=False, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=corrS_cols.values, xticklabels=corrS_cols.values)

fig.show()
fig.savefig('correlation.jpeg', bbox_inches='tight')


linreg(dt, dv)
ridgereg(dt, dv)


print("**************** normed and cleaned data ****************")
linreg(dt_s, dv_s)
ridgereg(dt_s, dv_s)

#its possible to see that normed  and cleaned data provide way better data